module github.com/instafreight/in-out-dash

go 1.15

require (
	github.com/PagerDuty/go-pagerduty v1.3.0
	github.com/bluele/slack v0.0.0-20180528010058-b4b4d354a079
	github.com/googollee/go-socket.io v1.4.4
	github.com/labstack/echo/v4 v4.1.17
	github.com/nlopes/slack v0.6.0
	github.com/slack-go/slack v0.8.0
	google.golang.org/appengine v1.6.7
)

package main

import "time"

//socketio "github.com/googollee/go-socket.io"
const (
	layoutISO = "2006-01-02T15:04:05"
	layoutUS  = "2006-01-02 15:04:05"
)

// RequestData struct containing informations comming from Slack
type RequestData struct {
	Token          string `json:"token" form:"token"`
	TeamID         string `json:"team_id" form:"team_id"`
	EnterpriseID   string `json:"enterprise_id" form:"enterprise_id"`
	EnterpriseName string `json:"enterprise_name" form:"enterprise_name"`
	ChannelID      string `json:"channel_id" form:"channel_id"`
	ChannelName    string `json:"channel_name" form:"channel_name"`
	UserID         string `json:"user_id" form:"user_id"`
	UserName       string `json:"user_name" form:"user_name"`
	Command        string `json:"command" form:"command"`
	Text           string `json:"text" form:"text"`
	ResponseURL    string `json:"response_url" form:"response_url"`
	TriggerID      string `json:"trigger_id" form:"trigger_id"`
	AppID          string `json:"api_app_id" form:"api_app_id"`
}

// UserStatus struct maps user updates comming from Slack
type UserStatus struct {
	UserID      string `json:"userId"`
	Status      string `json:"statuses"`
	Avatar      string `json:"avatar"`
	Name        string `json:"name"`
	UpdatedAT   string `json:"updatedAt"`
	Skype       string `json:"skype"`
	Email       string `json:"email"`
	StatusEmoji string `json:"statusEmoji"`
	StatusText  string `json:"statusText"`
	DisplayName string `json:"displayName"`
}

// Status struct of status update
type Status struct {
	Status    string    `json:"status"`
	Timestamp time.Time `json:"timestamp"`
}

// UserUpdates struct
type UserUpdates struct {
	Users []UserStatus
}

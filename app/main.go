package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/slack-go/slack"
)

func main() {

	uu := UserUpdates{}
	uu.Users = make([]UserStatus, 0)

	uu.Users = loadUsersFromFile()

	e := echo.New()
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	//CORS
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowMethods: []string{echo.GET, echo.HEAD, echo.PUT, echo.PATCH, echo.POST, echo.DELETE},
	}))
	// Root route => handler
	e.POST("/", func(c echo.Context) error {
		t := new(RequestData)
		c.Bind(t)
		uu.UpdateUser(t)
		return c.String(http.StatusOK, t.Text)
	})

	e.GET("/users", func(c echo.Context) error {
		return c.JSON(http.StatusOK, uu.Users)
	})

	e.GET("/clean", func(c echo.Context) error {
		uu = UserUpdates{}
		truncateFile()
		return c.NoContent(http.StatusOK)
	})

	e.GET("/access", func(c echo.Context) error {
		return c.HTML(http.StatusOK, "<a href='https://slack.com/oauth/v2/authorize?scope=incoming-webhook,users.profile:write:user&client_id=1222942165014.1657738390326&redirect_uri=http://localhost/return'>Access</a>")
	})

	/*
			e.Any("/socket.io/", func(c echo.Context) error {
				SocketStart()
				return nil
			})
		SocketStart()
	*/

	// Run Server
	e.Logger.Fatal(e.Start(":8019"))
	//e.Logger.Fatal(e.Start(":80"))
}

// UpdateUser actio
func (u *UserUpdates) UpdateUser(r *RequestData) {
	p := getUserProfile(r.UserID)
	us := UserStatus{
		UserID:      r.UserID,
		Name:        p.RealName,
		Avatar:      p.Image192,
		Status:      r.Text,
		Skype:       p.Skype,
		Email:       p.Email,
		StatusEmoji: p.StatusEmoji,
		StatusText:  p.StatusText,
		DisplayName: p.DisplayName,
	}
	t := time.Now()
	us.UpdatedAT = t.Format(layoutUS)

	hasItem := false
	aux := u.Users
	for i, item := range aux {
		if item.UserID == r.UserID {
			aux[i] = us
			hasItem = true
			break
		}
	}

	if hasItem == false {
		aux = append(aux, us)
	}

	u.Users = aux

	file, err := json.MarshalIndent(u.Users, "", " ")
	if err != nil {
		fmt.Println(err)
	}
	err = ioutil.WriteFile("users.json", file, 0755)
	if err != nil {
		fmt.Println(err)
	}
}

func truncateFile() {
	err := ioutil.WriteFile("users.json", []byte(""), 0755)
	if err != nil {
		fmt.Println(err)
	}
}

func loadUsersFromFile() []UserStatus {
	u := new([]UserStatus)
	b, _ := ioutil.ReadFile("users.json")
	json.Unmarshal(b, u)

	return *u
}

func getUserProfile(userID string) slack.UserProfile {
	//api := slack.New("xoxb-1222942165014-1670682362436-tx5oEwPMWnmvvxxhfXDCKoWV")
	api := slack.New("xoxb-19551373731-1666219253459-YX8MbWZdI4CkBwHFJ5MgeIcS")
	user, err := api.GetUserInfo(userID)

	//ProfileSet(userID)
	if err != nil {
		panic(err)
	}

	return user.Profile
}

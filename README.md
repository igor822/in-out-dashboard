# WIP: In-Out Dashboard

Simple Slack bot to show status of team that updated in slack

### Possible statuses

* `in`
* `out`
* `afk`

## Build

```sh
docker build -t registry.gitlab.com/igor822/in-out-dashboard/inout-app .
docker build -t registry.gitlab.com/igor822/in-out-dashboard/inout-dashboard .
```

And push to Container Registry

```sh
docker push registry.gitlab.com/igor822/in-out-dashboard/inout-app
docker push registry.gitlab.com/igor822/in-out-dashboard/inout-dashboard
```

## Run

```sh
docker run -p 8019:8019 -it registry.gitlab.com/igor822/in-out-dashboard/inout-app
docker run -p 8018:80 -it registry.gitlab.com/igor822/in-out-dashboard/inout-dashboard
```

## Test

```sh
curl -X POST -d '@data.txt' http://localhost:8019
```

And to access the dashboard just go to your browser and access [http://localhost:8018]


### Deployment

For deployment purposes it's necessary to add the following script in your server

```sh
#!/bin/bash

docker pull registry.gitlab.com/igor822/in-out-dashboard/inout-app
docker rm -f inout-app
docker run -d --name inout-app -p 8019:8019 -it registry.gitlab.com/igor822/in-out-dashboard/inout-app

docker pull registry.gitlab.com/igor822/in-out-dashboard/inout-dashboard
docker rm -f inout-dash
docker run -d --name inout-dash -p 8018:80 -it registry.gitlab.com/igor822/in-out-dashboard/inout-dashboard
```

import React from 'react';

function App({ children }: {children: any}) {
  return (
    <div className="dashboard-container container-fluid">
      {children}
    </div>
  );
}

export default App;

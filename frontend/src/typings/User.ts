export interface UserType {
  avatar: string;
  name: string;
  userId: string;
  statuses: string;
  updatedAt: string;
  skype: string;
  email: string;
  displayName: string;
}

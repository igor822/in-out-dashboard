import React, { useEffect, useState } from 'react';
import axios from 'axios';

import { UserType } from './typings/User';
import DashbaordLayout from './layouts/DashbaordLayout';
import User, { getRealStatus } from './components/User';
import Filter from './components/Filter';


const fetchUsers = async() => {
  try{
    return (await axios.get('http://108.61.204.148:8019/users')).data as UserType[];
  } catch(e) {
    console.log(e);
  }
  return [];
}

function App() {
  const [users, setUsers] = useState<UserType[]>([]);
  const [filters, setFilters] = useState<{search: string, filters: string[]}>({search: '', filters: []});

  const syncUsers = () =>{
    fetchUsers().then((usersList) =>{
      setUsers(usersList);
    });
  }

  const watchUpdates = () => {
    const timeout = setTimeout(()=>{
      syncUsers();
      watchUpdates();
    },3000);
    return timeout;
  }

  useEffect(() => {
    syncUsers();
    watchUpdates();
  }, []);

  return (
    <DashbaordLayout>
      <div className="row py-3">
        <div className="col-5 header">
          <img src={`/logo.svg`} alt="Insta" />
          <span>Tech In/Out</span>
        </div>
        <div className="col-7">
          <Filter onChange={(f) =>{
            setFilters(f);
          }} />
        </div>

      </div>
      <div className="row">
        {users
        .filter(user => filters.search ? `${user.name.toLowerCase()}`.indexOf(filters.search) > -1 : true)
        .filter(user => filters.filters.length > 0 ? filters.filters.includes(getRealStatus(user.statuses).status): true)
        .map(user => {
          return <User key={user.userId} user={user} />
        })}
      </div>
    </DashbaordLayout>
  );
}

export default App;

import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSlack, faSkype, faGoogle } from '@fortawesome/free-brands-svg-icons';
import { faPizzaSlice} from '@fortawesome/free-solid-svg-icons';
import { classMap } from './Filter';
import { UserType } from '../typings/User';
import './users.scss';
import moment from 'moment';

const StatusBadge = ({ status }: {status: string}) => {
  const possibleStatus = getRealStatus(status);
  return <>
    <span className={`badge badge-${classMap[possibleStatus.status] ? classMap[possibleStatus.status] : classMap.default}`}>{possibleStatus.status}</span>

    {possibleStatus.icon ? <span className="px-2" style={{color: '#74b731'}}><FontAwesomeIcon icon={possibleStatus.icon} /></span> : null}
  </>;
}

export function getRealStatus(statusText: string) {
  let status = statusText;

  for(let subText of statusText.split(/[\s,@]+/)){
    subText = subText.replace(/[\W_]+/g," ").toLocaleLowerCase().trim();

    if(['lunch', 'food'].includes(subText)) {
      return { status: 'afk', icon: faPizzaSlice };
    }
    
    if(['in', 'back', 'morning'].includes(subText)) {
      return { status: 'in' };
    }

    if(['afk'].includes(subText.replace(/[^a-zA-Z]+/, ''))) {
      return { status: 'afk' };
    }
  }

  return { status };
}

function User({ user }: { user: UserType}) {

  return (
    <div className="col-12 col-sm-6 col-md-4">
      <div className="card p-3">
        <div className="d-flex">
            <div className="image"> <img src={user.avatar || `https://randomuser.me/api/portraits/men/${Math.floor(Math.random()*10)}.jpg`} alt="" className="rounded" width="155" /> </div>
            <div className="ml-3">
                <h4 className="mb-0 mt-0">{user.name}{user.displayName ? <><small>({user.displayName})</small></> : ''} <br /> <StatusBadge status={user.statuses} />
</h4> <span>Senior software engineer</span>
                <p className="status-con">{user.statuses} since {moment(user.updatedAt).format('L LT')}</p>
                <div className="mt-2 d-flex flex-row"> 
                  <a 
                    className="social-icon p-1 px-2" 
                    href={`slack://user?team=T016JTQ4V0E&id=${user.userId}`}
                  >
                    <FontAwesomeIcon icon={faSlack}  size="2x" />
                  </a>
{                  user.skype ? <a 
                    className="social-icon p-1" 
                    href={`skype:${user.skype}?chat`}
                  >
                    <FontAwesomeIcon icon={faSkype}  size="2x" />
                  </a>: null}
{                  user.email ? <a 
                    className="social-icon p-1" 
                    href={`https://mail.google.com/mail/?view=cm&fs=1&tf=1&to=${user.email}`}
                    target="blank"
                  >
                    <FontAwesomeIcon icon={faGoogle}  size="2x" />
                  </a>: null}
                  
                </div>
            </div>
        </div>
    </div>
    </div>
  );
}

export default User;

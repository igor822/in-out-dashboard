import React, { useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import './filter.scss';

export const classMap = {
  in: 'success',
  afk: 'warning',
  out: 'danger',
  default: 'primary'
} as { [key: string]: string };

function Filter({ onChange = (f: any)=>{} }) {
  const [filters, setFilters] = useState<string[]>([]);
  const [search, setSearch] = useState<string>('');


  return <div className="filter-container col-12">
    <div className="status-filters">
      {Object.keys(classMap).map((key)=>{
        if(key === 'default') {
          return null;
        }

        return <button 
          key={key} 
          className={`${classMap[key]} ${filters.indexOf(key) >=0 ? `selected btn-${classMap[key]}`: 'btn-secondary'} btn`}
          onClick={() => {
            if(filters.indexOf(key)>=0) {
              filters.splice(filters.indexOf(key), 1);
            } else {
              filters.push(key);
            }
            setFilters([...filters]);
            onChange({filters, search});
          }}
        >{key}</button>
      })}
    </div>
    <div className="searchbar">
      <input className="search_input" type="text" name="" placeholder="Search..." onChange={({target})=>{
        setSearch(target.value);
        onChange({filters, search: target.value});
      }} />
      <a href="#" className="search_icon"><FontAwesomeIcon icon={faSearch} /></a>
    </div>
  </div>
}

export default Filter;
